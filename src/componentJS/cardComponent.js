import LabelComponent from "./labelComponent";
import TextAreaComponent from "./textAreaComponent";
import { useContext } from "react";
import { DataContext } from "../handlerJS/stateManagement";
import { useHistory } from "react-router-dom";

const CardComponent = () => {
  const { inputData, setInputData, popUpDetail, setPopUpDetail } =
    useContext(DataContext);
  const history = useHistory();
  return (
    <div className="border p-5 relative mt-16 rounded-lg gap-3 bg-slate-200 flex flex-col text-left justify-start items-center w-[36rem] h-[25rem]">
      <LabelComponent labelName="Title" />
      <TextAreaComponent
        value="title"
        className="h-16"
        inputValue={inputData.title}
      />
      <LabelComponent labelName="Description" />
      <TextAreaComponent
        value="body"
        className="h-36"
        inputValue={inputData.body}
      />
      <svg
        onClick={() => {
          history.push(`/`);
          setInputData({});
          setPopUpDetail(!popUpDetail);
        }}
        xmlns="http://www.w3.org/2000/svg"
        className="absolute bottom-5 right-5 h-8 w-8 cursor-pointer"
        viewBox="0 0 20 20"
        fill="currentColor"
      >
        <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
      </svg>
    </div>
  );
};

export default CardComponent;
