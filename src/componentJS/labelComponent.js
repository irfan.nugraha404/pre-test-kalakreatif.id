const LabelComponent = (props) => {
  return <label className="w-full"> {`${props.labelName}`} </label>;
};

export default LabelComponent;
