const NotificationComponent = (props) => {
  return (
    <div
      className={`${props.colorBackground} ${props.alertNotification} text-white absolute top-10 right-10 p-3 gap-2 flex flex-row origin-right duration-300`}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        className="h-6 w-6"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
        strokeWidth={2}
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
        />
      </svg>
      <span>{props.messageNotification}</span>
    </div>
  );
};

export default NotificationComponent;
