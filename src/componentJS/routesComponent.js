import { DataProvider } from "../handlerJS/stateManagement";
import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import HomePage from "../pagesJS/home";
import PrintPage from "../pagesJS/printPage";

const RoutesComponent = () => {
  return (
    <BrowserRouter>
      <DataProvider>
        <Switch>
          <Route exact path={`/`}>
            <HomePage />
          </Route>
          <Route exact path={`/print-page/:slug`}>
            <PrintPage />
          </Route>
        </Switch>
      </DataProvider>
    </BrowserRouter>
  );
};

export default RoutesComponent;
