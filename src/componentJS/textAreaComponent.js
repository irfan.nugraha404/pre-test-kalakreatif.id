import FunctionHandler from "../handlerJS/functionHandler";

const TextAreaComponent = (props) => {
  return (
    <textarea
      onChange={(e) => props.handle(`${props.value}`, e)}
      value={props.inputValue}
      className={`${props.className} border-4 text-sm border-dashed w-full px-2 py-1 focus:outline-none`}
    ></textarea>
  );
};

export default TextAreaComponent;
