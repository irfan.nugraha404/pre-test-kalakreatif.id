import FunctionHandler from "../handlerJS/functionHandler";
import { useContext } from "react";
import { DataContext } from "../handlerJS/stateManagement";
import LabelComponent from "./labelComponent";
import TextAreaComponent from "./textAreaComponent";

const CreateDataComponent = () => {
  const { handleCloseCD, handleSubmit, handleChangeCreate } = FunctionHandler();
  const { popUpCreateData, inputDataCreate } = useContext(DataContext);
  return (
    <div
      className={`absolute top-0 flex ${
        popUpCreateData ? "" : "scale-0"
      } w-full min-h-screen h-full justify-center items-center duration-300`}
    >
      <form
        method="post"
        className="relative w-[30rem] h-[30rem] bg-slate-200 origin-center p-5 gap-2 rounded-md drop-shadow-lg"
      >
        <button onClick={handleCloseCD} className="absolute top-2 right-2">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>
        <p className="pb-3">{`Detail`}</p>
        <LabelComponent labelName="Title" />
        <TextAreaComponent
          handle={handleChangeCreate}
          value="title"
          inputValue={inputDataCreate.title}
          className="h-16"
        />
        <LabelComponent labelName="Description" />
        <TextAreaComponent
          handle={handleChangeCreate}
          value="body"
          inputValue={inputDataCreate.body}
          className="h-32"
        />

        <div className="absolute gap-3 flex bottom-5 right-5 text-white">
          <button
            onClick={(e) => {
              e.preventDefault();
              if (inputDataCreate.title !== "") {
                handleChangeCreate("title", e);
              }
              if (inputDataCreate.body !== "") {
                handleChangeCreate("body", e);
              }
            }}
            className="p-2 bg-green-600 rounded-md"
          >
            Clear
          </button>
          <button onClick={handleCloseCD} className="p-2 bg-red-600 rounded-md">
            Cancel
          </button>
          <button onClick={handleSubmit} className="p-2 bg-blue-600 rounded-md">
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default CreateDataComponent;
