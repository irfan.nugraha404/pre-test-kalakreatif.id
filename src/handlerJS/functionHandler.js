import { useContext, useEffect } from "react";
import { DataContext } from "./stateManagement";
import axios from "axios";
import { useHistory } from "react-router-dom";

const FunctionHandler = () => {
  const {
    dataAPI,
    setDataAPI,
    inputData,
    setInputData,
    currentID,
    setCurrentID,
    popUpDetail,
    setPopUpDetail,
    setAlertUpdate,
    setAlertCreate,
    setAlertDelete,
    popUpCreateData,
    setPopUpCreateData,
    inputDataCreate,
    setInputDataCreate,
  } = useContext(DataContext);

  const history = useHistory();

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(
        `https://jsonplaceholder.typicode.com/posts`
      );
      setDataAPI(result.data);
    };
    fetchData();
  }, []);

  const handleDetail = (event) => {
    event.preventDefault();
    const clickedDetail = event.target.value;
    axios
      .get(`https://jsonplaceholder.typicode.com/posts/${clickedDetail}`)
      .then((res) => {
        setInputData(res.data);
        setCurrentID(res.data.id);
        setPopUpDetail(!popUpDetail);
      });
  };

  const handleChange = (key, event) => {
    setInputData({ ...inputData, [key]: event.target.value });
  };

  const handleChangeCreate = (key, event) => {
    setInputDataCreate({ ...inputDataCreate, [key]: event.target.value });
  };

  const handleEdit = (event) => {
    event.preventDefault();
    axios
      .put(`https://jsonplaceholder.typicode.com/posts/${currentID}`, inputData)
      .then((res) => {
        dataAPI[currentID - 1] = inputData;
        if (res.status === 200) {
          setAlertUpdate(true);
          handleAlertUpdate();
          setDataAPI([...dataAPI]);
        }
        setInputData({});
        setCurrentID(null);
        setPopUpDetail(!popUpDetail);
      });
  };

  const handleDelete = (event) => {
    event.preventDefault();
    axios
      .delete(`https://jsonplaceholder.typicode.com/posts/${currentID}`)
      .then((res) => {
        if (res.status === 200) {
          const filterDatabyID = dataAPI.filter((x) => x.id !== currentID);
          setDataAPI(filterDatabyID);
          setAlertDelete(true);
          handleAlertDelete();
        }
      });
    setInputData({});
    setCurrentID(null);
    setPopUpDetail(!popUpDetail);
  };

  const handlePrint = () => {
    history.push(`/print-page/${currentID}`);
  };

  const handleAlertUpdate = () => {
    setTimeout(() => {
      setAlertUpdate(false);
    }, 3000);
  };

  const handleAlertDelete = () => {
    setTimeout(() => {
      setAlertDelete(false);
    }, 3000);
  };

  const handleAlertCreate = () => {
    setTimeout(() => {
      setAlertCreate(false);
    }, 3000);
  };

  const handleCloseDetail = () => {
    setPopUpDetail(!popUpDetail);
    setInputData({});
  };

  const handleCloseCD = (event) => {
    event.preventDefault();
    setInputData({});
    setPopUpCreateData(!popUpCreateData);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    axios
      .post(`https://jsonplaceholder.typicode.com/posts`, inputDataCreate)
      .then((res) => {
        if (res.status === 201) {
          setDataAPI([...dataAPI, inputDataCreate]);
          setAlertCreate(true);
          handleAlertCreate();
        }
      });
    setInputDataCreate({});
    setPopUpCreateData(!popUpCreateData);
  };

  return {
    handleDetail,
    handleEdit,
    handleDelete,
    handleChange,
    handlePrint,
    handleCloseCD,
    handleCloseDetail,
    handleSubmit,
    handleChangeCreate,
  };
};
export default FunctionHandler;
