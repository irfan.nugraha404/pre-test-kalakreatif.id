import React, { createContext, useState } from "react";

export const DataContext = createContext();

export const DataProvider = (props) => {
  const [dataAPI, setDataAPI] = useState([]);
  const [inputData, setInputData] = useState({});
  const [inputDataCreate, setInputDataCreate] = useState({});
  const [currentID, setCurrentID] = useState(null);
  const [popUpDetail, setPopUpDetail] = useState(false);
  const [popUpCreateData, setPopUpCreateData] = useState(false);
  const [alertUpdate, setAlertUpdate] = useState(false);
  const [alertCreate, setAlertCreate] = useState(false);
  const [alertDelete, setAlertDelete] = useState(false);

  return (
    <DataContext.Provider
      value={{
        dataAPI,
        setDataAPI,
        inputData,
        setInputData,
        currentID,
        setCurrentID,
        popUpDetail,
        setPopUpDetail,
        alertUpdate,
        setAlertUpdate,
        alertCreate,
        setAlertCreate,
        alertDelete,
        setAlertDelete,
        popUpCreateData,
        setPopUpCreateData,
        inputDataCreate,
        setInputDataCreate,
      }}
    >
      {props.children}
    </DataContext.Provider>
  );
};
