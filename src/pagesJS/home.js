import React, { useContext } from "react";
import { DataContext } from "../handlerJS/stateManagement";
import FunctionHandler from "../handlerJS/functionHandler";
import NotificationComponent from "../componentJS/notificationComponent";
import DetailComponent from "../componentJS/detailComponent";
import CreateDataComponent from "../componentJS/createDataComponent";

const HomePage = () => {
  const {
    dataAPI,
    popUpDetail,
    alertUpdate,
    alertCreate,
    alertDelete,
    popUpCreateData,
    setPopUpCreateData,
    setInputDataCreate,
  } = useContext(DataContext);

  const { handleDetail } = FunctionHandler();

  return (
    <>
      {dataAPI !== null && (
        <>
          <div
            className={`${popUpDetail ? "blur-sm" : ""} ${
              popUpCreateData ? "blur-sm" : ""
            } w-full min-h-screen h-full p-10 flex flex-col items-center justify-center`}
          >
            <table className="w-[55rem] border-collapse text-left capitalize relative">
              <thead className="bg-slate-300">
                <tr>
                  <th colSpan={4} className="bg-white border-none">
                    <button
                      onClick={() => {
                        setPopUpCreateData(!popUpCreateData);
                        setInputDataCreate({});
                      }}
                      className="p-2 rounded-sm bg-blue-600 text-white flex"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        strokeWidth={2}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M12 6v6m0 0v6m0-6h6m-6 0H6"
                        />
                      </svg>
                      Create New Data
                    </button>
                  </th>
                </tr>
                <tr>
                  <th className="border px-2">ID</th>
                  <th className="border px-2">title</th>
                  <th className="border px-2">description</th>
                  <th className="border px-2">action</th>
                </tr>
              </thead>
              <tbody>
                {dataAPI.map((item, index) => {
                  let bgRow = "";
                  if (index % 2 === 1) {
                    bgRow = "bg-slate-100";
                  } else {
                    bgRow = "";
                  }
                  return (
                    <tr key={index} className={`${bgRow}`}>
                      <td className="border px-2">{item.id}</td>
                      <td className="border px-2">{`${item.title.slice(
                        0,
                        13
                      )}...`}</td>
                      <td className="border px-2">{`${item.body.slice(
                        0,
                        33
                      )}...`}</td>
                      <td className="border px-2">
                        <button
                          value={item.id}
                          onClick={handleDetail}
                          className="text-sm p-1 rounded-md bg-blue-500 text-white"
                        >
                          Detail
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <NotificationComponent
              colorBackground="bg-green-600"
              alertNotification={`${alertUpdate ? "" : "scale-0"}`}
              messageNotification={`Success! Data Updated`}
            />
            <NotificationComponent
              colorBackground="bg-red-600"
              alertNotification={`${alertDelete ? "" : "scale-0"}`}
              messageNotification={`Success! Data Deleted`}
            />
            <NotificationComponent
              colorBackground="bg-blue-600"
              alertNotification={`${alertCreate ? "" : "scale-0"}`}
              messageNotification={`Success! Data Created`}
            />
          </div>
          <DetailComponent />
          <CreateDataComponent />
        </>
      )}
    </>
  );
};

export default HomePage;
