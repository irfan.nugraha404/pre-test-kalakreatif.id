import { useEffect } from "react";
import CardComponent from "../componentJS/cardComponent";

const PrintPage = () => {
  useEffect(() => {
    alert("click home icon to go Back");
    window.print();
  }, []);
  return (
    <div className="flex justify-center items-start w-full min-h-screen h-full">
      <CardComponent />{" "}
    </div>
  );
};

export default PrintPage;
