import "./App.css";
import RoutesComponent from "./componentJS/routesComponent";

const App = () => {
  return <RoutesComponent />;
};

export default App;
